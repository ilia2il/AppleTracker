package ma.ifiag.com.classes;

public class MessageFireBase {

    private String value;
    private int status;

    public MessageFireBase() {}
    public MessageFireBase(String v, int s) {
        this.value = v;
        this.status = s;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getValue() {

        return value;
    }

    public int getStatus() {
        return status;
    }
}
