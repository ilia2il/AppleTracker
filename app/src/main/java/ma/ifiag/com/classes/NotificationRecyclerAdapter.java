package ma.ifiag.com.classes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import ma.ifiag.com.appletracker.R;

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.NotificationViewHolder> {
    private List<Notification> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context = null;

    public NotificationRecyclerAdapter (List<Notification> data, Context context) {
        this.data = data;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        view = this.inflater.inflate(R.layout.notification_activity_item, parent, false);
        NotificationViewHolder notificationViewHolder = new NotificationViewHolder(view);
        return notificationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        Notification notification = this.data.get(position);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy, HH:mm");

        holder.notificationHeaderText.setText(simpleDateFormat.format(notification.getDate()));
        holder.notificationBodyText.setText(notification.getMessage());
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        TextView notificationHeaderText;
        TextView notificationBodyText;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            notificationHeaderText = (TextView) itemView.findViewById(R.id.headerNotificationText);
            notificationBodyText = (TextView) itemView.findViewById(R.id.bodyNotificationText);
        }
    }
}
