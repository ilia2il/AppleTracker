package ma.ifiag.com.appletracker;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import ma.ifiag.com.classes.Constant;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private SupportMapFragment mapViewTracker;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private GoogleMap googleMap;
    private PlaceDetectionClient placeDetectionClient;
    private FusedLocationProviderClient fusedLocationProviderClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        getSupportActionBar().setTitle("Apple Tracker MAP");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.mapViewTracker = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.full_map_tracker);
        this.mapViewTracker.onCreate(savedInstanceState);
        this.mapViewTracker.getMapAsync(this);

        this.placeDetectionClient = Places.getPlaceDetectionClient(this, null);
        this.fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        this.locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                googleMap.clear();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
                googleMap.addMarker(markerOptions);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                myLastLocation();
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("POP", "onProviderDisabled Called");
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        };

        // GPS Permission
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, Constant.REQUEST_GPS_CODE);
            } else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, Constant.REQUEST_GPS_CODE);
            }
        } else {
            myRequestLocationUpdate();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == Constant.REQUEST_GPS_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            myRequestLocationUpdate();
        }
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void myRequestLocationUpdate () {
        Log.d("POP", "myRequestLocationUpdate Called");
        /*if (!this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if(!this.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, this.locationListener);
            } else {
                this.locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 0, this.locationListener);
            }

        } else {

        }*/

        this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, this.locationListener);
    }

    public void myLastLocation () {
        Task currentLocation = this.fusedLocationProviderClient.getLastLocation();
        currentLocation.addOnCompleteListener(this, new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if(task.isSuccessful() && task.getResult() != null) {
                    Location location = (Location) task.getResult();
                    Toast.makeText(getBaseContext(), String.valueOf(location.getLatitude()), Toast.LENGTH_SHORT).show();
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
                            location.getLatitude(), location.getLongitude()
                    )));
                } else {

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(33.5585692, -7.5843174));
                    googleMap.addMarker(markerOptions);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(
                            new LatLng(33.5585692, -7.5843174)
                    ));
                }
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mapViewTracker.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.mapViewTracker.onPause();
        Log.d("POP", "JE SUIS en Pause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mapViewTracker.onResume();
        Log.d("POP", "JE SUIS en relancer");
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.mapViewTracker.onStart();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        this.mapViewTracker.onLowMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mapViewTracker.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            myLastLocation();
        }
        googleMap.setMinZoomPreference(15);
        //Toast.makeText(this, "Map ready", Toast.LENGTH_SHORT).show();
    }
}
