package ma.ifiag.com.appletracker;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Random;

import ma.ifiag.com.classes.Constant;
import ma.ifiag.com.classes.MessageFireBase;

public class NotificationService extends Service {

    private NotificationCompat.Builder notificationBuilder;
    private Context context = getBaseContext();
    private NotificationManager notificationManager;
    private DatabaseReference databaseReference;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        databaseReference = FirebaseDatabase.getInstance().getReference();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        databaseReference.child("notifications").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                MessageFireBase msg = dataSnapshot.getValue(MessageFireBase.class);
                if(msg.getStatus() == 0) {
                    showNotification(msg.getValue());
                    dataSnapshot.getRef().child("status").setValue(1);
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return super.onStartCommand(intent, flags, startId);
    }

    // Show Notification
    public void showNotification (String msg) {

        Intent intent = new Intent(this, NotificationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        this.notificationBuilder = new NotificationCompat.Builder(this, Constant.CHANNEL_ID);
        this.notificationBuilder
                .setSmallIcon(R.drawable.ic_apple_tracker_notif)
                .setVibrate(new long[] {1000, 1000})
                .setContentTitle(Constant.NOTIFICATION_TITLE)
                .setContentText(msg)
                .setShowWhen(true)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(true)
                .setLights(Color.BLUE, 3000, 3000)
                .setGroup(Constant.NOTIFICATION_GROUP)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent);

        this.notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
        this.notificationManager.notify(Calendar.getInstance().get(Calendar.SECOND) + new Random().nextInt(10000), this.notificationBuilder.build());
    }
}
