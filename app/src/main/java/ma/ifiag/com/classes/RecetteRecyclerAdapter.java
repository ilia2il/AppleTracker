package ma.ifiag.com.classes;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.net.URI;
import java.util.List;

import ma.ifiag.com.appletracker.R;

public class RecetteRecyclerAdapter extends RecyclerView.Adapter<RecetteRecyclerAdapter.RecetteViewHolder> {

    private List<Recette> data;
    private LayoutInflater layoutInflater;
    private Context context;

    public RecetteRecyclerAdapter (List<Recette> data, Context context) {
        this.data = data;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecetteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = this.layoutInflater.inflate(R.layout.recette_activity_item, parent, false);
        RecetteViewHolder recetteViewHolder = new RecetteViewHolder(view);
        return recetteViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecetteViewHolder holder, int position) {
        final Recette recette = this.data.get(position);

        holder.recetteTitle.setText(recette.getTitle());
        holder.recetteSubTitle.setText(recette.getSmallTitle());
        holder.recetteImage.setImageResource(recette.getImage());
        holder.recetteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(recette.getLink()));
                v.getContext().startActivity(browser);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    public class RecetteViewHolder extends RecyclerView.ViewHolder {

        TextView recetteTitle;
        TextView recetteSubTitle;
        ImageView recetteImage;

        public RecetteViewHolder(View itemView) {
            super(itemView);
            recetteTitle = (TextView) itemView.findViewById(R.id.recette_title);
            recetteSubTitle = (TextView) itemView.findViewById(R.id.recette_sub_title);
            recetteImage = (ImageView) itemView.findViewById(R.id.recette_image);
        }
    }

}
