package ma.ifiag.com.classes;

import java.util.Date;

public class Notification {

    private Date date;
    private String message;

    public Notification () {}
    public Notification(Date date, String message) {
        this.date = date;
        this.message = message;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {

        return date;
    }

    public String getMessage() {
        return message;
    }
}
