package ma.ifiag.com.appletracker;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ma.ifiag.com.classes.MessageFireBase;
import ma.ifiag.com.classes.Notification;
import ma.ifiag.com.classes.NotificationRecyclerAdapter;

public class NotificationActivity extends AppCompatActivity {

    private RecyclerView notificationRecyclerView;
    private NotificationRecyclerAdapter notificationRecyclerAdapter;
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private DatabaseReference databaseReference;
    public static int NOTIFICATIONID = 1990;
    private Context context;
    private List<Notification> notificationList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notifications");
        setContentView(R.layout.activity_notification);

        notificationRecyclerView = (RecyclerView) findViewById(R.id.notification_recycler_view);

        this.notificationList = new ArrayList<>();
        notificationRecyclerAdapter = new NotificationRecyclerAdapter(notificationList, getApplicationContext());
        notificationRecyclerView.setAdapter(notificationRecyclerAdapter);
        notificationRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));

        // Show Data in Notification View
        this.databaseReference = FirebaseDatabase.getInstance().getReference();
        this.databaseReference.child("notifications").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MessageFireBase messageFireBase = dataSnapshot.getValue(MessageFireBase.class);
                Notification notification = new Notification(new Date(), messageFireBase.getValue());
                notificationList.add(notification);
                notificationRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
