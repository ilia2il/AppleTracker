package ma.ifiag.com.appletracker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ma.ifiag.com.classes.Recette;
import ma.ifiag.com.classes.RecetteRecyclerAdapter;

public class RecetteActivity extends AppCompatActivity {

    private RecyclerView recetteRecyclerView;
    private RecetteRecyclerAdapter recetteRecyclerAdapter;
    private List<Recette> recetteList;

    private Recette recette1;
    //private Recette recette2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("RECETTES");
        setContentView(R.layout.activity_recette);

        recetteRecyclerView = (RecyclerView) findViewById(R.id.recette_recycler_view);
        this.recetteList = new ArrayList<>();

        this.recette1 = new Recette("Gâteau aux pommes rapide", "Dessert", R.drawable.recette, "http://www.marmiton.org/recettes/recette_gateau-aux-pommes-rapide_37577.aspx");
        //this.recette2 = new Recette();

        this.recetteList.add(this.recette1);
        //this.recetteList.add(this.recette2);

        this.recetteRecyclerAdapter = new RecetteRecyclerAdapter(this.recetteList, getApplicationContext());
        recetteRecyclerView.setAdapter(this.recetteRecyclerAdapter);
        recetteRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
    }
}
