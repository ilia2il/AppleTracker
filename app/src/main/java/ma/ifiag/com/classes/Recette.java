package ma.ifiag.com.classes;

public class Recette {

    private String title;
    private String smallTitle;
    private int image;
    private String link;

    public Recette(){}



    public Recette (String title, String smallTitle, int image, String link) {
        this.title = title;
        this.smallTitle = smallTitle;
        this.image = image;
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSmallTitle(String smallTitle) {
        this.smallTitle = smallTitle;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {

        return title;
    }

    public String getSmallTitle() {
        return smallTitle;
    }

    public int getImage() {
        return image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
