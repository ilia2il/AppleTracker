package ma.ifiag.com.appletracker;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import at.grabner.circleprogress.CircleProgressView;
import ma.ifiag.com.classes.Constant;

public class MainActivity extends AppCompatActivity {


    private Context context;
    private CircleProgressView circleProgressView;
    private Button buttonMap;
    private Button buttonRecette;
    private DatabaseReference databaseReference;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this, NotificationService.class));

        databaseReference = FirebaseDatabase.getInstance().getReference();
        context = getApplicationContext();
        circleProgressView = (CircleProgressView) findViewById(R.id.apple_circular_wiew);

        databaseReference.child("stock").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                updateCircleProgressView(dataSnapshot.getValue(int.class));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                updateCircleProgressView(dataSnapshot.getValue(int.class));
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        this.buttonRecette = (Button) findViewById(R.id.show_recette);
        this.buttonRecette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recetteIntent = new Intent(v.getContext(), RecetteActivity.class);
                startActivity(recetteIntent);
            }
        });

        this.buttonMap = (Button) findViewById(R.id.show_map);
        this.buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(v.getContext(), MapActivity.class);
                startActivity(mapIntent);
            }
        });

        // Get GPS Permission
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, Constant.REQUEST_GPS_CODE);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == Constant.REQUEST_GPS_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        }
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.tracker_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.parametre:
                Intent parametre = new Intent(this, ParametreActivity.class);
                startActivity(parametre);
                return true;
            case R.id.notifcation:
                Intent notification = new Intent(this, NotificationActivity.class);
                startActivity(notification);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }



    // Circle View Data
    public void updateCircleProgressView (int value) {
        float percentage = (float) value / 1000;
        float progressValue = (percentage * 100);
        //circleProgressView.setValue(progressValue);
        circleProgressView.setValueAnimated(progressValue);
        if(progressValue > 50) {
            this.buttonMap.setVisibility(View.GONE);
            this.buttonRecette.setVisibility(View.VISIBLE);
            circleProgressView.setBarColor(getResources().getColor(R.color.progressSuccessBar));
            circleProgressView.setTextColor(getResources().getColor(R.color.progressSuccessText));
            circleProgressView.setUnitColor(getResources().getColor(R.color.progressSuccessText));
        } else {
            this.buttonMap.setVisibility(View.VISIBLE);
            this.buttonRecette.setVisibility(View.GONE);
            circleProgressView.setBarColor(getResources().getColor(R.color.progressDangerBar));
            circleProgressView.setTextColor(getResources().getColor(R.color.progressDangerBar));
            circleProgressView.setUnitColor(getResources().getColor(R.color.progressDangerBar));
        }
    }

    // Create Notification Channel
    public void createNotifiacationChannel () {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel notificationChannel = new NotificationChannel(Constant.CHANNEL_ID, Constant.CHAR_SEQUENCE, NotificationManager.IMPORTANCE_HIGH);
            this.notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            this.notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    // Show Notification
    public void showNotification (String msg) {
        this.notificationBuilder = new NotificationCompat.Builder(this.context, Constant.CHANNEL_ID);
        this.notificationBuilder
                .setSmallIcon(R.drawable.ic_apple_tracker_notif)
                .setVibrate(new long[] {1000, 1000})
                .setContentTitle(Constant.NOTIFICATION_TITLE)
                .setContentText(msg)
                .setShowWhen(true)
                .setColor(ContextCompat.getColor(this.context, R.color.colorPrimary))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(true)
                .setLights(Color.RED, 3000, 3000);

        this.notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        this.notificationManager.notify(50, this.notificationBuilder.build());
    }

}
