package ma.ifiag.com.classes;

public class Constant {

    public static String CHANNEL_ID = "APPLETRACKERNOTIF0017";
    public static CharSequence CHAR_SEQUENCE = "APPLETRACKER";
    public static String NOTIFICATION_TITLE = "Apple Tracker";
    public static int REQUEST_GPS_CODE = 350;
    public static String NOTIFICATION_GROUP = "APPLE_TRACKER_NOTIF";
}
